def test_required_input_param(client):
    response = client.get("/api/v1/cheapest/")
    assert response.status_code == 400
    assert response.json["error"] == "O parâmetro input é obrigatório." 

def test_invalid_input_format(client):
    response = client.get("/api/v1/cheapest/?input=regular16Mar2009(mon)")
    assert response.status_code == 400
    assert "Entrada inválida" in response.json["error"]

def test_invalid_date_format(client):
    response = client.get("/api/v1/cheapest/?input=regular:Mar162009(mon)")
    assert response.status_code == 400
    assert "Formato de data inválido" in response.json["error"] 

def test_validate_client_type(client):
    response = client.get("/api/v1/cheapest/?input=plus:16Mar2009(mon)")
    assert response.status_code == 400
    assert response.json["error"] == "Tipo de cliente inválido. Tipos válidos: Regular, Reward." 

def test_calculate_cheapest(client):
    response = client.get("/api/v1/cheapest/?input=regular:16Mar2009(mon)")
    assert response.status_code == 200
    assert response.json["cheapest"] == "Lakewood" 

def test_calculate_cheapest_for_multiple_dates(client):
    response = client.get("/api/v1/cheapest/?input=regular:16Mar2009(mon), 17Mar2009(tues)")
    assert response.status_code == 200
    assert response.json["cheapest"] == "Lakewood" 
