Essa aplicação de teste responde dentre os locais inseridos no JSON qual é o mais barato para se hospedar, a depender do dia (se é semana ou final de semana) e/ou do tipo de cliente (regular ou especial). Para rodar:

 - Instale as dependências do projeto:
```shell
pip install -r requirements.txt
```
- Execute os testes
```shell
python -m pytest -vx
```
 - Execute a aplicação:
```shell
python main.py
```
Feito isso a aplicação estará servindo no endereço http://localhost:5000/ ou http://localhost:5000/api/v1/cheapest/

Para testar a aplicação, é necessário passar alguns parâmetros na URL, como o tipo de cliente (regular/reward) e a data ou período desejado para hospedagem no formato 15Ago2021(). Exemplo:

http://localhost:5000/api/v1/cheapest/?input=regular: 29Mar2021() 

Obs.: Aplicação utilizada apenas para testar o CI do gitlab para a disciplina de devops da pós-graduação.