import re
from flask import Flask, request, jsonify
from datetime import datetime


hotels = [
    {
        'name': 'Lakewood',
        'rating': 3,
        'week_rate': {
            'regular': 110,
            'reward': 80
        },
        'weekend_rate': {
            'regular': 90,
            'reward': 80
        }
    },
    {
        'name': 'Bridgewood',
        'rating': 4,
        'week_rate': {
            'regular': 160,
            'reward': 110
        },
        'weekend_rate': {
            'regular': 60,
            'reward': 50
        }
    },
    {
        'name': 'Ridgewood',
        'rating': 5,
        'week_rate': {
            'regular': 220,
            'reward': 100
        },
        'weekend_rate': {
            'regular': 150,
            'reward': 40
        }
    }
]


def calculate_cheapest(client_type, dates):
    cheapest_price = None
    cheapest_name = None
    cheapest_rating = None

    for hotel in hotels:
        hotel_price = 0
        for date in dates:
            date_type = 'week_rate' if date.weekday() < 5 else 'weekend_rate'
            hotel_price += hotel[date_type][client_type]

        if cheapest_price is None or hotel_price < cheapest_price:
            cheapest_price = hotel_price
            cheapest_name = hotel['name']
            cheapest_rating = hotel['rating']
        elif hotel_price == cheapest_price:
            if hotel['rating'] > cheapest_rating:
                cheapest_price = hotel_price
                cheapest_name = hotel['name']
                cheapest_rating = hotel['rating']

    return {'cheapest': cheapest_name}


def response(input_data):
    if not input_data:
        return {'error': 'O parâmetro input é obrigatório.'}, 400

    match = re.match(r"(.*):(.*)", input_data)
    if match:
        client_type, dates = match.groups()
        formatted_client_type = client_type.lower().strip()
        if formatted_client_type not in ['regular', 'reward']:
            return {'error': 'Tipo de cliente inválido. Tipos válidos: Regular, Reward.'}, 400

        try:
            booking_dates = [date.replace(' ', '') for date in dates.split(',')]
            booking_dates = [
                datetime.strptime(re.match(r"(.*)\([a-z]*\)", date).groups()[0], "%d%b%Y")
                for date in booking_dates
            ]

            result = calculate_cheapest(formatted_client_type, booking_dates)
            return result, 200
        except (AttributeError, ValueError):
            return {'error': 'Formato de data inválido. Formatos válidos seguem o padrão: '
                             '16Mar2009(mon), 17Mar2009(tues), ...'}, 400

    return {'error': 'Entrada inválida. O formato dos dados de entrada deve ser: '
                     '<tipo_do_cliente>: <data1>, <data2>, <data3>, ...'}, 400



def create_app():
    app = Flask(__name__)

    @app.route('/api/v1/cheapest/', methods=['GET'])
    def cheapest():
        input_data = request.args.get('input')
        result, status = response(input_data)

        return jsonify(result), status

    return app


if __name__ == '__main__':
    app = create_app()
    app.run(host="0.0.0.0", debug=False)